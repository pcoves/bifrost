use super::route::Route;
use yew::prelude::*;
use yew_router::prelude::*;

#[function_component]
pub fn Header() -> Html {
    html! {
        <div class={classes!("header")}>
            <div class={classes!("header__brand")}>
                <Link<Route> to={Route::Root}>
                    <img class={classes!("header__brand__image")} src="img/rainbow.png" alt="Bifröst" />
                </Link<Route>>
            </div>
            <div class={classes!("header__content")}>
                <div class={classes!("header__content__title")}>
                    { "Bifröst" }
                </div>
                <div class={classes!("header__content__description")}>
                    { "Yet another color picker" }
                </div>
            </div>
        </div>
    }
}
