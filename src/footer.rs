use yew::prelude::*;

#[function_component]
pub fn Footer() -> Html {
    html! {
        <div class={classes!("footer")}>
            <span>{"Made with "}<a href="https://yew.rs/">{"Yew"}</a></span>
            <span>{"By "}<a href="https://pcoves.gitlab.io">{"P. COVES"}</a></span>
            <span>{ "Under "}<a href="http://www.wtfpl.net/">{"WTFPL"}</a></span>
        </div>
    }
}
