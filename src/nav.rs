use super::route::Route;
use crate::pages::schemes::Route as SchemesRoute;
use yew::prelude::*;
use yew_router::prelude::*;

#[function_component]
pub fn Nav() -> Html {
    html! {
        <div class={classes!("nav")}>
            <Link<SchemesRoute> to={SchemesRoute::Monochromatic}>{"Monochromatic"}</Link<SchemesRoute>>
            <Link<SchemesRoute> to={SchemesRoute::Analogous}>{"Analogous"}</Link<SchemesRoute>>
            <Link<SchemesRoute> to={SchemesRoute::Complementary}>{"Complementary"}</Link<SchemesRoute>>
            <Link<SchemesRoute> to={SchemesRoute::Triadic}>{"Triadic"}</Link<SchemesRoute>>
            <Link<SchemesRoute> to={SchemesRoute::Compound}>{"Compound"}</Link<SchemesRoute>>
            <Link<Route> to={Route::Contrast}>{"Contrast calculator"}</Link<Route>>
        </div>
    }
}
