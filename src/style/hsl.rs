#[derive(Clone, Copy, PartialEq, serde::Deserialize, serde::Serialize)]
pub struct Hsl {
    pub hue: u16,
    pub saturation: u8,
    pub lightness: u8,
}
