mod hsl;

pub use hsl::Hsl;

#[derive(Clone, Copy, PartialEq, serde::Deserialize, serde::Serialize)]
pub struct Style {
    pub primary: Hsl,
    pub primary_alt: Hsl,

    pub secondary: Hsl,
    pub secondary_alt: Hsl,

    pub tertiary: Hsl,
    pub tertiary_alt: Hsl,
}

// impl Style {
//     pub fn set_primary_hue(&self, hue: u16) -> Self {
//         Self {
//             primary: Hsl {
//                 hue,
//                 ..self.primary
//             },
//             ..*self
//         }
//     }
//     pub fn set_primary_saturation(&self, saturation: u8) -> Self {
//         Self {
//             primary: Hsl {
//                 saturation,
//                 ..self.primary
//             },
//             ..*self
//         }
//     }
//     pub fn set_primary_lightness(&self, lightness: u8) -> Self {
//         Self {
//             primary: Hsl {
//                 lightness,
//                 ..self.primary
//             },
//             ..*self
//         }
//     }

//     pub fn set_primary_alt_hue(&self, hue: u16) -> Self {
//         Self {
//             primary_alt: Hsl {
//                 hue,
//                 ..self.primary_alt
//             },
//             ..*self
//         }
//     }
//     pub fn set_primary_alt_saturation(&self, saturation: u8) -> Self {
//         Self {
//             primary_alt: Hsl {
//                 saturation,
//                 ..self.primary_alt
//             },
//             ..*self
//         }
//     }
//     pub fn set_primary_alt_lightness(&self, lightness: u8) -> Self {
//         Self {
//             primary_alt: Hsl {
//                 lightness,
//                 ..self.primary_alt
//             },
//             ..*self
//         }
//     }

//     pub fn set_secondary_hue(&self, hue: u16) -> Self {
//         Self {
//             secondary: Hsl {
//                 hue,
//                 ..self.secondary
//             },
//             ..*self
//         }
//     }
//     pub fn set_secondary_saturation(&self, saturation: u8) -> Self {
//         Self {
//             secondary: Hsl {
//                 saturation,
//                 ..self.secondary
//             },
//             ..*self
//         }
//     }
//     pub fn set_secondary_lightness(&self, lightness: u8) -> Self {
//         Self {
//             secondary: Hsl {
//                 lightness,
//                 ..self.secondary
//             },
//             ..*self
//         }
//     }

//     pub fn set_secondary_alt_hue(&self, hue: u16) -> Self {
//         Self {
//             secondary_alt: Hsl {
//                 hue,
//                 ..self.secondary_alt
//             },
//             ..*self
//         }
//     }
//     pub fn set_secondary_alt_saturation(&self, saturation: u8) -> Self {
//         Self {
//             secondary_alt: Hsl {
//                 saturation,
//                 ..self.secondary_alt
//             },
//             ..*self
//         }
//     }
//     pub fn set_secondary_alt_lightness(&self, lightness: u8) -> Self {
//         Self {
//             secondary_alt: Hsl {
//                 lightness,
//                 ..self.secondary_alt
//             },
//             ..*self
//         }
//     }

//     pub fn set_tertiary_hue(&self, hue: u16) -> Self {
//         Self {
//             tertiary: Hsl {
//                 hue,
//                 ..self.tertiary
//             },
//             ..*self
//         }
//     }
//     pub fn set_tertiary_saturation(&self, saturation: u8) -> Self {
//         Self {
//             tertiary: Hsl {
//                 saturation,
//                 ..self.tertiary
//             },
//             ..*self
//         }
//     }
//     pub fn set_tertiary_lightness(&self, lightness: u8) -> Self {
//         Self {
//             tertiary: Hsl {
//                 lightness,
//                 ..self.tertiary
//             },
//             ..*self
//         }
//     }

//     pub fn set_tertiary_alt_hue(&self, hue: u16) -> Self {
//         Self {
//             tertiary_alt: Hsl {
//                 hue,
//                 ..self.tertiary_alt
//             },
//             ..*self
//         }
//     }
//     pub fn set_tertiary_alt_saturation(&self, saturation: u8) -> Self {
//         Self {
//             tertiary_alt: Hsl {
//                 saturation,
//                 ..self.tertiary_alt
//             },
//             ..*self
//         }
//     }
//     pub fn set_tertiary_alt_lightness(&self, lightness: u8) -> Self {
//         Self {
//             tertiary_alt: Hsl {
//                 lightness,
//                 ..self.tertiary_alt
//             },
//             ..*self
//         }
//     }
// }

impl Default for Style {
    fn default() -> Self {
        Self {
            primary: Hsl {
                hue: 220,
                saturation: 30,
                lightness: 90,
            },
            primary_alt: Hsl {
                hue: 220,
                saturation: 40,
                lightness: 80,
            },

            secondary: Hsl {
                hue: 20,
                saturation: 25,
                lightness: 25,
            },
            secondary_alt: Hsl {
                hue: 20,
                saturation: 10,
                lightness: 35,
            },

            tertiary: Hsl {
                hue: 230,
                saturation: 65,
                lightness: 50,
            },
            tertiary_alt: Hsl {
                hue: 230,
                saturation: 65,
                lightness: 50,
            },
        }
    }
}

impl std::fmt::Display for Style {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "
                --primary-hue: {}; --primary-sat: {}%; --primary-lig: {}%;
                --primary-alt-hue: {}; --primary-alt-sat: {}%; --primary-alt-lig: {}%;
                --secondary-hue: {}; --secondary-sat: {}%; --secondary-lig: {}%;
                --secondary-alt-hue: {}; --secondary-alt-sat: {}%; --secondary-alt-lig: {}%;
                --tertiary-hue: {}; --tertiary-sat: {}%; --tertiary-lig: {}%;
                --tertiary-alt-hue: {}; --tertiary-alt-sat: {}%; --tertiary-alt-lig: {}%;
            ",
            self.primary.hue,
            self.primary.saturation,
            self.primary.lightness,
            self.primary_alt.hue,
            self.primary_alt.saturation,
            self.primary_alt.lightness,
            self.secondary.hue,
            self.secondary.saturation,
            self.secondary.lightness,
            self.secondary_alt.hue,
            self.secondary_alt.saturation,
            self.secondary_alt.lightness,
            self.tertiary.hue,
            self.tertiary.saturation,
            self.tertiary.lightness,
            self.tertiary_alt.hue,
            self.tertiary_alt.saturation,
            self.tertiary_alt.lightness,
        )
    }
}
