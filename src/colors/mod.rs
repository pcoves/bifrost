mod hsl;
mod luminance;
mod rgb;

pub use self::{hsl::Hsl, luminance::Luminance, rgb::Rgb};
use std::{
    cmp::{max, min},
    convert::From,
};

impl From<Hsl> for Rgb {
    fn from(hsl: Hsl) -> Self {
        let (h, s, l) = (
            f32::from(hsl.hue),
            f32::from(hsl.saturation) / 100f32,
            f32::from(hsl.lightness) / 100f32,
        );

        let c = (1. - (2. * l - 1.).abs()) * s;
        let x = c * (1. - (((h / 60.) % 2.) - 1.).abs());

        let [red, green, blue] = match h {
            h if h < 60. => [c, x, 0.],
            h if h < 120. => [x, c, 0.],
            h if h < 180. => [0., c, x],
            h if h < 240. => [0., x, c],
            h if h < 300. => [x, 0., c],
            _ => [c, 0., x],
        }
        .map(|x| ((x + l - c / 2.) * 255f32) as u8);

        Self { red, green, blue }
    }
}

impl From<Rgb> for Hsl {
    fn from(rgb: Rgb) -> Self {
        let min = min(rgb.red, min(rgb.green, rgb.blue));
        let max = max(rgb.red, max(rgb.green, rgb.blue));

        let delta = f32::from(max - min) / 255f32;

        let (r, g, b) = (
            f32::from(rgb.red) / 255f32,
            f32::from(rgb.green) / 255f32,
            f32::from(rgb.blue) / 255f32,
        );

        let hue = (60.
            * if delta == 0. {
                0.
            } else if max == rgb.red {
                (((g - b) / delta) + 6f32) % 6f32
            } else if max == rgb.green {
                ((b - r) / delta) + 2.
            } else {
                ((r - g) / delta) + 4.
            })
        .round() as u16;

        let lightness = (f32::from(min) / 255f32 + f32::from(max) / 255f32) / 2f32;

        let saturation = if delta == 0. {
            0.
        } else {
            delta / (1. - (2. * lightness - 1.).abs())
        };

        Self {
            hue,
            saturation: (saturation * 100f32).round() as u8,
            lightness: (lightness * 100f32).round() as u8,
        }
    }
}

impl From<Rgb> for Luminance {
    fn from(rgb: Rgb) -> Self {
        let f = |x: f32| {
            if x < 0.03928 {
                x / 12.92
            } else {
                ((x + 0.055) / 1.055).powf(2.4)
            }
        };

        Luminance(
            0.2126 * f(f32::from(rgb.red) / 255f32)
                + 0.7152 * f(f32::from(rgb.green) / 255f32)
                + 0.0722 * f(f32::from(rgb.blue) / 255f32),
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn colors() {
        {
            let hsl = Hsl {
                hue: 0,
                saturation: 0,
                lightness: 0,
            };

            let rgb = Rgb {
                red: 0,
                green: 0,
                blue: 0,
            };

            assert_eq!(Rgb::from(hsl.clone()), rgb);
            assert_eq!(hsl, Hsl::from(rgb.clone()));
        }
        {
            let hsl = Hsl {
                hue: 0,
                saturation: 0,
                lightness: 100,
            };

            let rgb = Rgb {
                red: 255,
                green: 255,
                blue: 255,
            };

            assert_eq!(Rgb::from(hsl.clone()), rgb);
            assert_eq!(hsl, Hsl::from(rgb.clone()));
        }
        {
            let hsl = Hsl {
                hue: 0,
                saturation: 100,
                lightness: 50,
            };

            let rgb = Rgb {
                red: 255,
                green: 0,
                blue: 0,
            };

            assert_eq!(Rgb::from(hsl.clone()), rgb);
            assert_eq!(hsl, Hsl::from(rgb.clone()));
        }
        {
            let hsl = Hsl {
                hue: 120,
                saturation: 100,
                lightness: 50,
            };

            let rgb = Rgb {
                red: 0,
                green: 255,
                blue: 0,
            };

            assert_eq!(Rgb::from(hsl.clone()), rgb);
            assert_eq!(hsl, Hsl::from(rgb.clone()));
        }
        {
            let hsl = Hsl {
                hue: 240,
                saturation: 100,
                lightness: 50,
            };

            let rgb = Rgb {
                red: 0,
                green: 0,
                blue: 255,
            };

            assert_eq!(Rgb::from(hsl.clone()), rgb);
            assert_eq!(hsl, Hsl::from(rgb.clone()));
        }
        {
            let hsl = Hsl {
                hue: 60,
                saturation: 100,
                lightness: 50,
            };

            let rgb = Rgb {
                red: 255,
                green: 255,
                blue: 0,
            };

            assert_eq!(Rgb::from(hsl.clone()), rgb);
            assert_eq!(hsl, Hsl::from(rgb.clone()));
        }
        {
            let hsl = Hsl {
                hue: 180,
                saturation: 100,
                lightness: 50,
            };

            let rgb = Rgb {
                red: 0,
                green: 255,
                blue: 255,
            };

            assert_eq!(Rgb::from(hsl.clone()), rgb);
            assert_eq!(hsl, Hsl::from(rgb.clone()));
        }
        {
            let hsl = Hsl {
                hue: 300,
                saturation: 100,
                lightness: 50,
            };

            let rgb = Rgb {
                red: 255,
                green: 0,
                blue: 255,
            };

            assert_eq!(Rgb::from(hsl.clone()), rgb);
            assert_eq!(hsl, Hsl::from(rgb.clone()));
        }
        {
            let hsl = Hsl {
                hue: 0,
                saturation: 0,
                lightness: 75,
            };

            let rgb = Rgb {
                red: 191,
                green: 191,
                blue: 191,
            };

            assert_eq!(Rgb::from(hsl.clone()), rgb);
            assert_eq!(hsl, Hsl::from(rgb.clone()));
        }
        {
            let hsl = Hsl {
                hue: 0,
                saturation: 0,
                lightness: 75,
            };

            let rgb = Rgb {
                red: 191,
                green: 191,
                blue: 191,
            };

            assert_eq!(Rgb::from(hsl.clone()), rgb);
            assert_eq!(hsl, Hsl::from(rgb.clone()));
        }
        {
            let hsl = Hsl {
                hue: 0,
                saturation: 0,
                lightness: 75,
            };

            let rgb = Rgb {
                red: 191,
                green: 191,
                blue: 191,
            };

            assert_eq!(Rgb::from(hsl.clone()), rgb);
            assert_eq!(hsl, Hsl::from(rgb.clone()));
        }
        {
            let hsl = Hsl {
                hue: 0,
                saturation: 0,
                lightness: 75,
            };

            let rgb = Rgb {
                red: 191,
                green: 191,
                blue: 191,
            };

            assert_eq!(Rgb::from(hsl.clone()), rgb);
            assert_eq!(hsl, Hsl::from(rgb.clone()));
        }
        {
            let hsl = Hsl {
                hue: 0,
                saturation: 0,
                lightness: 50,
            };

            let rgb = Rgb {
                red: 127,
                green: 127,
                blue: 127,
            };

            assert_eq!(Rgb::from(hsl.clone()), rgb);
            assert_eq!(hsl, Hsl::from(rgb.clone()));
        }
        {
            let hsl = Hsl {
                hue: 0,
                saturation: 100,
                lightness: 25,
            };

            let rgb = Rgb {
                red: 127,
                green: 0,
                blue: 0,
            };

            assert_eq!(Rgb::from(hsl.clone()), rgb);
            assert_eq!(hsl, Hsl::from(rgb.clone()));
        }
        {
            let hsl = Hsl {
                hue: 60,
                saturation: 100,
                lightness: 25,
            };

            let rgb = Rgb {
                red: 127,
                green: 127,
                blue: 0,
            };

            assert_eq!(Rgb::from(hsl.clone()), rgb);
            assert_eq!(hsl, Hsl::from(rgb.clone()));
        }
        {
            let hsl = Hsl {
                hue: 120,
                saturation: 100,
                lightness: 25,
            };

            let rgb = Rgb {
                red: 0,
                green: 127,
                blue: 0,
            };

            assert_eq!(Rgb::from(hsl.clone()), rgb);
            assert_eq!(hsl, Hsl::from(rgb.clone()));
        }
        {
            let hsl = Hsl {
                hue: 300,
                saturation: 100,
                lightness: 25,
            };

            let rgb = Rgb {
                red: 127,
                green: 0,
                blue: 127,
            };

            assert_eq!(Rgb::from(hsl.clone()), rgb);
            assert_eq!(hsl, Hsl::from(rgb.clone()));
        }
        {
            let hsl = Hsl {
                hue: 180,
                saturation: 100,
                lightness: 25,
            };

            let rgb = Rgb {
                red: 0,
                green: 127,
                blue: 127,
            };

            assert_eq!(Rgb::from(hsl.clone()), rgb);
            assert_eq!(hsl, Hsl::from(rgb.clone()));
        }
        {
            let hsl = Hsl {
                hue: 240,
                saturation: 100,
                lightness: 25,
            };

            let rgb = Rgb {
                red: 0,
                green: 0,
                blue: 127,
            };

            assert_eq!(Rgb::from(hsl.clone()), rgb);
            assert_eq!(hsl, Hsl::from(rgb.clone()));
        }
    }
}
