#[derive(Clone, PartialEq)]
pub struct Luminance(pub f32);

impl Luminance {
    pub fn contrast(&self, other: &Self) -> f32 {
        let f = |l, r| (l + 0.05) / (r + 0.05);

        let Luminance(lhs) = &self;
        let Luminance(rhs) = &other;

        if lhs < rhs {
            f(rhs, lhs)
        } else {
            f(lhs, rhs)
        }
    }
}
