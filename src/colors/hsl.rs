use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct Hsl {
    pub hue: u16,
    pub saturation: u8,
    pub lightness: u8,
}
