use crate::colors::{Hsl, Luminance, Rgb};
use yew::prelude::*;
use yew_hooks::prelude::*;

const LHS: Hsl = Hsl {
    hue: 200,
    saturation: 30,
    lightness: 90,
};

const RHS: Hsl = Hsl {
    hue: 200,
    saturation: 25,
    lightness: 20,
};

#[function_component]
pub fn Contrast() -> Html {
    let output = use_node_ref();

    let lhs_hsl = use_local_storage::<Hsl>("lhs".to_string());
    let rhs_hsl = use_local_storage::<Hsl>("rhs".to_string());

    let lhs_rgb = use_state(|| Rgb::from((*lhs_hsl).clone().unwrap_or(LHS)));
    let rhs_rgb = use_state(|| Rgb::from((*rhs_hsl).clone().unwrap_or(RHS)));

    let lhs_luminance = use_state(|| Luminance::from((*lhs_rgb).clone()));
    let rhs_luminance = use_state(|| Luminance::from((*rhs_rgb).clone()));

    let contrast = use_state(|| lhs_luminance.contrast(&rhs_luminance));

    let lhs_set_hsl = Callback::from({
        let lhs_hsl = lhs_hsl.clone();
        let lhs_rgb = lhs_rgb.clone();
        move |hsl: Hsl| {
            lhs_hsl.set(hsl.clone());
            lhs_rgb.set(Rgb::from(hsl));
        }
    });

    let rhs_set_hsl = Callback::from({
        let rhs_hsl = rhs_hsl.clone();
        let rhs_rgb = rhs_rgb.clone();
        move |hsl: Hsl| {
            rhs_hsl.set(hsl.clone());
            rhs_rgb.set(Rgb::from(hsl));
        }
    });

    let lhs_set_rgb = Callback::from({
        let lhs_hsl = lhs_hsl.clone();
        let lhs_rgb = lhs_rgb.clone();
        move |rgb: Rgb| {
            lhs_rgb.set(rgb.clone());
            lhs_hsl.set(Hsl::from(rgb));
        }
    });

    let rhs_set_rgb = Callback::from({
        let rhs_hsl = rhs_hsl.clone();
        let rhs_rgb = rhs_rgb.clone();
        move |rgb: Rgb| {
            rhs_rgb.set(rgb.clone());
            rhs_hsl.set(Hsl::from(rgb));
        }
    });

    use_effect_with_deps(
        {
            let lhs_luminance = lhs_luminance.clone();
            move |rgb: &Rgb| lhs_luminance.set(Luminance::from(rgb.clone()))
        },
        (*lhs_rgb).clone(),
    );

    use_effect_with_deps(
        {
            let rhs_luminance = rhs_luminance.clone();
            move |rgb: &Rgb| rhs_luminance.set(Luminance::from(rgb.clone()))
        },
        (*rhs_rgb).clone(),
    );

    use_effect_with_deps(
        {
            let lhs_hsl = lhs_hsl.clone();
            let rhs_hsl = rhs_hsl.clone();

            let contrast = contrast.clone();
            move |(lhs, rhs): &(Luminance, Luminance)| {
                contrast.set(lhs.contrast(&rhs));

                let lhs_hsl = (*lhs_hsl).as_ref().unwrap_or(&LHS);
                let rhs_hsl = (*rhs_hsl).as_ref().unwrap_or(&RHS);

                let style = web_sys::Document::new().unwrap().body().unwrap().style();
                let _ = style.set_property(
                    "--primary",
                    &format!(
                        "hsl({}, {}%, {}%)",
                        lhs_hsl.hue, lhs_hsl.saturation, lhs_hsl.lightness
                    ),
                );
                let _ = style.set_property(
                    "--primary-alt",
                    &format!(
                        "hsl({}, {}%, {}%)",
                        lhs_hsl.hue, lhs_hsl.saturation, lhs_hsl.lightness
                    ),
                );
                let _ = style.set_property(
                    "--secondary",
                    &format!(
                        "hsl({}, {}%, {}%)",
                        rhs_hsl.hue, rhs_hsl.saturation, rhs_hsl.lightness
                    ),
                );
                let _ = style.set_property(
                    "--secondary-alt",
                    &format!(
                        "hsl({}, {}%, {}%)",
                        rhs_hsl.hue, rhs_hsl.saturation, rhs_hsl.lightness
                    ),
                );
                let _ = style.set_property(
                    "--tertiary",
                    &format!(
                        "hsl({}, {}%, {}%)",
                        rhs_hsl.hue, rhs_hsl.saturation, rhs_hsl.lightness
                    ),
                );
                let _ = style.set_property(
                    "--tertiary-alt",
                    &format!(
                        "hsl({}, {}%, {}%)",
                        rhs_hsl.hue, rhs_hsl.saturation, rhs_hsl.lightness
                    ),
                );

                || {
                    let style = web_sys::Document::new().unwrap().body().unwrap().style();
                    let _ = style.remove_property("--primary");
                    let _ = style.remove_property("--primary-alt");
                    let _ = style.remove_property("--secondary");
                    let _ = style.remove_property("--secondary-alt");
                    let _ = style.remove_property("--tertiary");
                    let _ = style.remove_property("--tertiary-alt");
                }
            }
        },
        ((*lhs_luminance).clone(), (*rhs_luminance).clone()),
    );

    html! {
        <div class={classes!("index")}>
            <header>
                <h1>{"Contrast calculator"}</h1>
            </header>

            <section class={classes!("pickers")}>
                <div class={classes!("pickers__input")}>
                    <picker::Picker hsl={(*lhs_hsl).clone().unwrap_or(LHS)} setter_hsl={lhs_set_hsl.clone()} rgb={(*lhs_rgb).clone()} setter_rgb={lhs_set_rgb.clone()} luminance={(*lhs_luminance).clone()} />
                    <picker::Picker hsl={(*rhs_hsl).clone().unwrap_or(RHS)} setter_hsl={rhs_set_hsl.clone()} rgb={(*rhs_rgb).clone()} setter_rgb={rhs_set_rgb.clone()} luminance={(*rhs_luminance).clone()} />
                </div>
                <div ref={output} class={classes!("pickers__output")}>
                    {format!("Contrast: {:.3}", *contrast)}
                    if *contrast < 4.5 {
                        {" which is not high enough for accessible content."}
                    } else if *contrast < 7. {
                        {" which is OK for big texts but not small ones."}
                    } else {
                        {" which is OK in all circumstances."}
                    }
                </div>
            </section>
        </div>
    }
}

mod picker {
    use crate::colors::{Hsl, Luminance, Rgb};
    use yew::prelude::*;

    #[function_component]
    pub fn Picker(
        Props {
            hsl,
            setter_hsl,
            rgb,
            setter_rgb,
            luminance,
        }: &Props,
    ) -> Html {
        html! {
            <div class={classes!("picker")}>
                <hsl::Hsl hsl={hsl.clone()} setter={setter_hsl.clone()} />
                <rgb::Rgb rgb={rgb.clone()} setter={setter_rgb.clone()} />
                <hex::Hex rgb={rgb.clone()} setter={setter_rgb.clone()} />
                <div class={classes!("picker__luminance")}>
                    {format!("Luminance: {:.3}", luminance.0)}
                </div>
            </div>
        }
    }

    #[derive(Properties, PartialEq)]
    pub struct Props {
        pub hsl: Hsl,
        pub rgb: Rgb,
        pub setter_hsl: Callback<Hsl>,
        pub setter_rgb: Callback<Rgb>,
        pub luminance: Luminance,
    }

    mod hsl {
        use web_sys::HtmlInputElement;
        use yew::prelude::*;

        #[function_component]
        pub fn Hsl(Props { hsl, setter }: &Props) -> Html {
            let hue = use_node_ref();
            let saturation = use_node_ref();
            let lightness = use_node_ref();

            let set_hue = Callback::from({
                let hsl = hsl.clone();
                let setter = setter.clone();

                let hue = hue.clone();

                move |_| {
                    let hue = hue
                        .cast::<HtmlInputElement>()
                        .unwrap()
                        .value()
                        .parse::<u16>()
                        .unwrap();

                    setter.emit(crate::colors::Hsl { hue, ..hsl })
                }
            });

            let set_saturation = Callback::from({
                let hsl = hsl.clone();
                let setter = setter.clone();

                let saturation = saturation.clone();

                move |_| {
                    let saturation = saturation
                        .cast::<HtmlInputElement>()
                        .unwrap()
                        .value()
                        .parse::<u8>()
                        .unwrap();

                    setter.emit(crate::colors::Hsl { saturation, ..hsl })
                }
            });

            let set_lightness = Callback::from({
                let hsl = hsl.clone();
                let setter = setter.clone();

                let lightness = lightness.clone();

                move |_| {
                    let lightness = lightness
                        .cast::<HtmlInputElement>()
                        .unwrap()
                        .value()
                        .parse::<u8>()
                        .unwrap();

                    setter.emit(crate::colors::Hsl { lightness, ..hsl })
                }
            });

            html! {
                <pre>
                    {"HSL("}
                    <input ref={hue} type="number" min="0" max="359" value={hsl.hue.to_string()} oninput={set_hue.clone()} />
                    {", "}
                    <input ref={saturation} type="number" min="0" max="100" value={hsl.saturation.to_string()} oninput={set_saturation.clone()} />
                    {", "}
                    <input ref={lightness} type="number" min="0" max="100" value={hsl.lightness.to_string()} oninput={set_lightness.clone()} />
                    {");"}
                </pre>
            }
        }

        #[derive(Properties, PartialEq)]
        pub struct Props {
            pub hsl: crate::colors::Hsl,
            pub setter: Callback<crate::colors::Hsl>,
        }
    }

    mod rgb {
        use web_sys::HtmlInputElement;
        use yew::prelude::*;

        #[function_component]
        pub fn Rgb(Props { rgb, setter }: &Props) -> Html {
            let red = use_node_ref();
            let green = use_node_ref();
            let blue = use_node_ref();

            let set_red = Callback::from({
                let rgb = rgb.clone();
                let setter = setter.clone();

                let red = red.clone();

                move |_| {
                    let red = red
                        .cast::<HtmlInputElement>()
                        .unwrap()
                        .value()
                        .parse::<u8>()
                        .unwrap();

                    setter.emit(crate::colors::Rgb { red, ..rgb })
                }
            });

            let set_green = Callback::from({
                let rgb = rgb.clone();
                let setter = setter.clone();

                let green = green.clone();

                move |_| {
                    let green = green
                        .cast::<HtmlInputElement>()
                        .unwrap()
                        .value()
                        .parse::<u8>()
                        .unwrap();

                    setter.emit(crate::colors::Rgb { green, ..rgb })
                }
            });

            let set_blue = Callback::from({
                let rgb = rgb.clone();
                let setter = setter.clone();

                let blue = blue.clone();

                move |_| {
                    let blue = blue
                        .cast::<HtmlInputElement>()
                        .unwrap()
                        .value()
                        .parse::<u8>()
                        .unwrap();

                    setter.emit(crate::colors::Rgb { blue, ..rgb })
                }
            });

            html! {
                <pre>
                    {"RGB("}
                    <input ref={red} type="number" min="0" max="255" value={rgb.red.to_string()} oninput={set_red.clone()} />
                    {", "}
                    <input ref={green} type="number" min="0" max="255" value={rgb.green.to_string()} oninput={set_green.clone()} />
                    {", "}
                    <input ref={blue} type="number" min="0" max="255" value={rgb.blue.to_string()} oninput={set_blue.clone()} />
                    {");"}
                </pre>
            }
        }

        #[derive(Properties, PartialEq)]
        pub struct Props {
            pub rgb: crate::colors::Rgb,
            pub setter: Callback<crate::colors::Rgb>,
        }
    }
    mod hex {
        use web_sys::HtmlInputElement;
        use yew::prelude::*;

        #[function_component]
        pub fn Hex(Props { rgb, setter }: &Props) -> Html {
            let hex = use_node_ref();

            let set_hex = Callback::from({
                let setter = setter.clone();

                let hex = hex.clone();

                move |_| {
                    let input = hex.cast::<HtmlInputElement>().unwrap().value();

                    if input.starts_with('#') && input.len() == 7 {
                        if let Ok(hex) = u32::from_str_radix(input.trim_start_matches('#'), 16) {
                            let red = ((hex >> 16) & 255) as u8;
                            let green = ((hex >> 8) & 255) as u8;
                            let blue = (hex & 255) as u8;

                            setter.emit(crate::colors::Rgb { red, green, blue })
                        }
                    }
                }
            });

            html! {
                <pre>
                    {"HEX:"}
                    <input ref={hex} value={format!("#{:02x}{:02x}{:02x}", rgb.red, rgb.green, rgb.blue)} oninput={set_hex.clone()} />
                </pre>
            }
        }

        #[derive(Properties, PartialEq)]
        pub struct Props {
            pub rgb: crate::colors::Rgb,
            pub setter: Callback<crate::colors::Rgb>,
        }
    }
}
