use yew::prelude::*;
use yew_router::prelude::*;

#[function_component]
pub fn Root() -> Html {
    html! {
    <article class={classes!("page", "flow")}>
        <header>
            <h1>{"Home"}</h1>
        </header>

        <p>{"Hey there, I'm "}<a href="https://pcoves.gitlab.io">{"P. COVES"}</a>{" and I make ugly things that work®. "}</p>

        <p>
            {"I needed a tool to help me pick some colors. "}
            {"So I first made "}<a href="https://pcoves.gitlab.io/rainbow/">{"Rainbow"}</a>{" in "}<i>{"plain old "}</i><code>{"HTML/CSS/JS"}</code>{". "}
            {"I then realized I could do much better and so I rewrote it in "}<code>{"Rust"}</code>{" as all things should be®."}
        </p>

        <p>
            {"You can pick one of the five color schemes generator and make wonders. "}
            {"Or you can compare two colors in the "}<Link<crate::Route> to={crate::Route::Contrast}>{"Contrast calculator"}</Link<crate::Route>>{". "}
        </p>

        <p>
            {"Hope it comes in handy for someone, one day, maybe. Enjoy !"}
        </p>
    </article>
    }
}
