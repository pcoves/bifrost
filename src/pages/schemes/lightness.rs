use crate::{
    colors::{Hsl, Luminance, Rgb},
    Color,
};
use web_sys::Element;
use yew::prelude::*;

#[function_component]
pub fn Lightness(Props { color }: &Props) -> Html {
    let lightness = |mouse_event: MouseEvent| -> u8 {
        let surface = mouse_event
            .target_dyn_into::<Element>()
            .unwrap()
            .get_bounding_client_rect();

        (100f64 * (f64::from(mouse_event.client_x()) - surface.left())
            / (surface.right() - surface.left()))
        .round() as u8
    };

    let onmousemove = Callback::from({
        let hsl = color.hsl.clone();
        let set_preview = color.previewer.clone();

        move |mouse_event: MouseEvent| {
            set_preview.emit(Some(Hsl {
                lightness: lightness(mouse_event),
                ..hsl
            }));
        }
    });

    let onclick = Callback::from({
        let hsl = color.hsl.clone();
        let set_hsl = color.setter.clone();

        move |mouse_event: MouseEvent| {
            set_hsl.emit(Hsl {
                lightness: lightness(mouse_event),
                ..hsl
            })
        }
    });

    let onmouseout = Callback::from({
        let set_preview = color.previewer.clone();
        move |_: MouseEvent| set_preview.emit(None)
    });

    let contrast = Luminance::from(Rgb::from(Hsl {
        lightness: 50,
        ..color.preview.as_ref().unwrap_or(&color.hsl).clone()
    }))
    .contrast(&Luminance(0f32));

    html! {
        <div {onmousemove} {onclick} {onmouseout}
        class={
            classes!("lightness",
            format!("lightness--{}", if let Some(ref hsl) = color.preview { hsl.hue } else { color.hsl.hue }),
            format!("sat--{}", if let Some(ref hsl) = color.preview { hsl.saturation } else {color.hsl.saturation}),
            if contrast < 10.5 { "color--white" } else { "color--black" }
        ) } >
        {
        format!("{}%",
            if let Some(ref preview) = color.preview {
                preview.lightness
            } else {
                color.hsl.lightness
            })
        }
        </div>
    }
}

#[derive(Properties, PartialEq)]
pub struct Props {
    pub color: Color,
}
