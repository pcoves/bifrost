use crate::colors::Hsl;
use web_sys::Element;
use yew::prelude::*;

#[function_component]
pub fn Hue(
    Props {
        hsl,
        set_hsl,
        set_preview,
    }: &Props,
) -> Html {
    let hue = |mouse_event: MouseEvent| -> u16 {
        let surface = mouse_event
            .target_dyn_into::<Element>()
            .unwrap()
            .get_bounding_client_rect();

        (360f64 * (f64::from(mouse_event.client_x()) - surface.left())
            / (surface.right() - surface.left()))
        .round() as u16
    };

    let onmousemove = Callback::from({
        let hsl = hsl.clone();
        let set_preview = set_preview.clone();

        move |mouse_event: MouseEvent| {
            set_preview.emit(Some(Hsl {
                hue: hue(mouse_event),
                ..hsl
            }));
        }
    });

    let onclick = Callback::from({
        let hsl = hsl.clone();
        let set_hsl = set_hsl.clone();

        move |mouse_event: MouseEvent| {
            set_hsl.emit(Hsl {
                hue: hue(mouse_event),
                ..hsl
            })
        }
    });

    let onmouseout = Callback::from({
        let set_preview = set_preview.clone();
        move |_: MouseEvent| set_preview.emit(None)
    });

    html! { <div class={classes!("hue")} {onmousemove} {onclick} {onmouseout} ></div> }
}

#[derive(Properties, PartialEq)]
pub struct Props {
    pub hsl: Hsl,
    pub set_hsl: Callback<Hsl>,
    pub set_preview: Callback<Option<Hsl>>,
}
