use super::{hue::Hue, lightness::Lightness, saturation::Saturation};
use crate::{colors::Hsl, Color, Contrasts};
use yew::prelude::*;

#[function_component]
pub fn Monochromatic(
    Props {
        primary,
        primary_alt,
        secondary,
        secondary_alt,
        tertiary,
        tertiary_alt,
        contrasts,
    }: &Props,
) -> Html {
    let set_primary = Callback::from({
        let set_primary = primary.setter.clone();

        let primary_alt = primary_alt.clone();
        let set_primary_alt = primary_alt.setter.clone();

        let secondary = secondary.clone();
        let set_secondary = secondary.setter.clone();

        let secondary_alt = secondary_alt.clone();
        let set_secondary_alt = secondary_alt.setter.clone();

        let tertiary = tertiary.clone();
        let set_tertiary = tertiary.setter.clone();

        let tertiary_alt = tertiary_alt.clone();
        let set_tertiary_alt = tertiary_alt.setter.clone();

        move |Hsl {
                  hue,
                  saturation,
                  lightness,
              }| {
            set_primary.emit(Hsl {
                hue,
                saturation,
                lightness,
            });
            set_primary_alt.emit(Hsl {
                hue,
                ..primary_alt.hsl
            });

            set_secondary.emit(Hsl {
                hue,
                ..secondary.hsl
            });
            set_secondary_alt.emit(Hsl {
                hue,
                ..secondary_alt.hsl
            });

            set_tertiary.emit(Hsl {
                hue,
                ..tertiary.hsl
            });
            set_tertiary_alt.emit(Hsl {
                hue,
                ..tertiary_alt.hsl
            });
        }
    });

    let set_primary_preview = Callback::from({
        let set_primary_preview = primary.previewer.clone();

        let set_primary_alt_preview = primary_alt.previewer.clone();
        let primary_alt = primary_alt.hsl.clone();

        let set_secondary_preview = secondary.previewer.clone();
        let secondary = secondary.clone();

        let set_secondary_alt_preview = secondary_alt.previewer.clone();
        let secondary_alt = secondary_alt.hsl.clone();

        let set_tertiary_preview = tertiary_alt.previewer.clone();
        let tertiary = tertiary.hsl.clone();

        let set_tertiary_alt_preview = tertiary_alt.previewer.clone();
        let tertiary_alt = tertiary_alt.hsl.clone();

        move |option_hsl| {
            if let Some(Hsl { hue, .. }) = option_hsl {
                set_primary_preview.emit(option_hsl);
                set_primary_alt_preview.emit(Some(Hsl { hue, ..primary_alt }));
                set_secondary_preview.emit(Some(Hsl {
                    hue,
                    ..secondary.hsl
                }));
                set_secondary_alt_preview.emit(Some(Hsl {
                    hue,
                    ..secondary_alt
                }));
                set_tertiary_preview.emit(Some(Hsl { hue, ..tertiary }));
                set_tertiary_alt_preview.emit(Some(Hsl {
                    hue,
                    ..tertiary_alt
                }));
            } else {
                set_primary_preview.emit(None);
                set_primary_alt_preview.emit(None);
                set_secondary_preview.emit(None);
                set_secondary_alt_preview.emit(None);
                set_tertiary_preview.emit(None);
                set_tertiary_alt_preview.emit(None);
            }
        }
    });

    html! {
        <article class={classes!("page", "monochromatic", "flow")}>
            <header>
                <h1>{"Monochromatic"}</h1>
            </header>

            <section class={classes!("backgrounds", "flow")}>
                <h2>{"Backgrounds"}</h2>

                <section class={classes!("primary")}>
                    <h3>{"Primary"}</h3>

                    <blockquote>
                        <pre>
                            if let Some(ref primary) = primary.preview {
                                {format!("--primary: hsl({}, {}%, {}%);", primary.hue, primary.saturation, primary.lightness)}
                            } else {
                                {format!("--primary: hsl({}, {}%, {}%);", primary.hsl.hue, primary.hsl.saturation, primary.hsl.lightness)}
                            }
                        </pre>

                        <p>{format!("Contrast is {:.2}:1 with foreground and {:.2}:1 with accent.", contrasts.primary_secondary, contrasts.primary_tertiary)}</p>

                        <ul>
                            <li>{"For body text, 4.5:1 is the minimum ratio while 7:1 is for enhanced accessibility."}</li>
                            <li>{"For large text, 3:1 is the minimum ratio while 4.5:1 is for enhanced accessibility."}</li>
                        </ul>
                    </blockquote>

                    <div>
                        <p>{"Pick the primary color :"}</p>
                        <Hue hsl={primary.hsl.clone()} set_hsl={set_primary.clone()} set_preview={set_primary_preview.clone()} />
                    </div>

                    <div>
                        <p>{"Tune its saturation :"}</p>
                        <Saturation color={primary.clone()} />
                    </div>

                    <div>
                        <p>{"And its lightness :"}</p>
                        <Lightness color={primary.clone()} />
                    </div>
                </section>

                <section class={classes!("primary", "primary--alt", "flow")}>
                    <h3>{"Primary Alt"}</h3>

                    <blockquote>
                        <pre>
                            if let Some(ref primary_alt) = primary_alt.preview {
                                {format!("--primary-alt: hsl({}, {}%, {}%);", primary_alt.hue, primary_alt.saturation, primary_alt.lightness)}
                            } else {
                                {format!("--primary-alt: hsl({}, {}%, {}%);", primary_alt.hsl.hue, primary_alt.hsl.saturation, primary_alt.hsl.lightness)}
                            }
                        </pre>

                        <p>{format!("Contrast is {:.2}:1 with alt foreground and {:.2}:1 with alt accent.", contrasts.primary_alt_secondary_alt, contrasts.primary_alt_tertiary_alt)}</p>

                        <ul>
                            <li>{"For body text, 4.5:1 is the minimum ratio while 7:1 is for enhanced accessibility."}</li>
                            <li>{"For large text, 3:1 is the minimum ratio while 4.5:1 is for enhanced accessibility."}</li>
                        </ul>
                    </blockquote>

                    <div>
                        <p>{"Tune its saturation :"}</p>
                        <Saturation color={primary_alt.clone()} />
                    </div>

                    <div>
                        <p>{"And its lightness :"}</p>
                        <Lightness color={primary_alt.clone()} />
                    </div>
                </section>
            </section>

            <section class={classes!("foregrounds", "flow")}>
                <h2>{"Foregrounds"}</h2>

                <section class={classes!("secondary", "flow")}>
                    <h3>{"Secondary"}</h3>

                    <blockquote>
                        <pre>
                            if let Some(ref secondary) = secondary.preview {
                                {format!("--secondary: hsl({}, {}%, {}%);", secondary.hue, secondary.saturation, secondary.lightness)}
                            } else {
                                {format!("--secondary: hsl({}, {}%, {}%);", secondary.hsl.hue, secondary.hsl.saturation, secondary.hsl.lightness)}
                            }
                        </pre>

                        <p>{format!("Contrast is {:.2}:1 with background.", contrasts.primary_secondary)}</p>

                        <ul>
                            <li>{"For body text, 4.5:1 is the minimum ratio while 7:1 is for enhanced accessibility."}</li>
                            <li>{"For large text, 3:1 is the minimum ratio while 4.5:1 is for enhanced accessibility."}</li>
                        </ul>
                    </blockquote>

                    <div>
                        <p>{"Tune its saturation :"}</p>
                        <Saturation color={secondary.clone()} />
                    </div>

                    <div>
                        <p>{"And its lightness :"}</p>
                        <Lightness color={secondary.clone()} />
                    </div>
                </section>

                <section class={classes!("secondary", "secondary--alt", "flow")}>
                    <h3>{"Secondary Alt"}</h3>

                    <blockquote>
                        <pre>
                            if let Some(ref secondary_alt) = secondary_alt.preview {
                                {format!("--secondary-alt: hsl({}, {}%, {}%);", secondary_alt.hue, secondary_alt.saturation, secondary_alt.lightness)}
                            } else {
                                {format!("--secondary-alt: hsl({}, {}%, {}%);", secondary_alt.hsl.hue, secondary_alt.hsl.saturation, secondary_alt.hsl.lightness)}
                            }
                        </pre>

                        <p>{format!("Contrast is {:.2}:1 with alt background.", contrasts.primary_alt_secondary_alt)}</p>

                        <ul>
                            <li>{"For body text, 4.5:1 is the minimum ratio while 7:1 is for enhanced accessibility."}</li>
                            <li>{"For large text, 3:1 is the minimum ratio while 4.5:1 is for enhanced accessibility."}</li>
                        </ul>
                    </blockquote>

                    <div>
                        <p>{"Tune its saturation :"}</p>
                        <Saturation color={secondary_alt.clone()} />
                    </div>

                    <div>
                        <p>{"And its lightness :"}</p>
                        <Lightness color={secondary_alt.clone()} />
                    </div>
                </section>
            </section>

            <section class={classes!("accents", "flow")}>
                <h2>{"Accents"}</h2>

                <section class={classes!("tertiary", "flow")}>
                    <h3>{"Tertiary"}</h3>

                    <blockquote>
                        <pre>
                            if let Some(ref tertiary) = tertiary.preview {
                                {format!("--tertiary: hsl({}, {}%, {}%);", tertiary.hue, tertiary.saturation, tertiary.lightness)}
                            } else {
                                {format!("--tertiary: hsl({}, {}%, {}%);", tertiary.hsl.hue, tertiary.hsl.saturation, tertiary.hsl.lightness)}
                            }
                        </pre>

                        <p>{format!("Contrast is {:.2}:1 with background.", contrasts.primary_tertiary)}</p>

                        <ul>
                            <li>{"For body text, 4.5:1 is the minimum ratio while 7:1 is for enhanced accessibility."}</li>
                            <li>{"For large text, 3:1 is the minimum ratio while 4.5:1 is for enhanced accessibility."}</li>
                        </ul>
                    </blockquote>

                    <div>
                        <p>{"Tune its saturation :"}</p>
                        <Saturation color={tertiary.clone()} />
                    </div>

                    <div>
                        <p>{"And its lightness :"}</p>
                        <Lightness color={tertiary.clone()} />
                    </div>
                </section>

                <section class={classes!("tertiary", "tertiary--alt", "flow")}>
                    <h3>{"Tertiary Alt"}</h3>

                    <blockquote>
                        <pre>
                            if let Some(ref tertiary_alt) = tertiary_alt.preview {
                                {format!("--tertiary-alt: hsl({}, {}%, {}%);", tertiary_alt.hue, tertiary_alt.saturation, tertiary_alt.lightness)}
                            } else {
                                {format!("--tertiary-alt: hsl({}, {}%, {}%);", tertiary_alt.hsl.hue, tertiary_alt.hsl.saturation, tertiary_alt.hsl.lightness)}
                            }
                        </pre>

                        <p>{format!("Contrast is {:.2}:1 with alt background.", contrasts.primary_alt_tertiary_alt)}</p>

                        <ul>
                            <li>{"For body text, 4.5:1 is the minimum ratio while 7:1 is for enhanced accessibility."}</li>
                            <li>{"For large text, 3:1 is the minimum ratio while 4.5:1 is for enhanced accessibility."}</li>
                        </ul>
                    </blockquote>

                    <div>
                        <p>{"Tune its saturation :"}</p>
                        <Saturation color={tertiary_alt.clone()} />
                    </div>

                    <div>
                        <p>{"And its lightness :"}</p>
                        <Lightness color={tertiary_alt.clone()} />
                    </div>
                </section>
            </section>
        </article>
    }
}

#[derive(Properties, PartialEq)]
pub struct Props {
    pub primary: Color,
    pub primary_alt: Color,
    pub secondary: Color,
    pub secondary_alt: Color,
    pub tertiary: Color,
    pub tertiary_alt: Color,
    pub contrasts: Contrasts,
}
