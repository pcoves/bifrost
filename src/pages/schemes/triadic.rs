use super::{hue::Hue, lightness::Lightness, saturation::Saturation};
use crate::{colors::Hsl, Color, Contrasts};
use yew::prelude::*;
use yew_hooks::use_local_storage;

const PRIMARY_ALT_TRIADIC: u16 = 0;
const SECONDARY_TRIADIC: u16 = 120;
const SECONDARY_ALT_TRIADIC: u16 = 120;
const TERTIARY_TRIADIC: u16 = 240;
const TERTIARY_ALT_TRIADIC: u16 = 240;

#[function_component]
pub fn Triadic(
    Props {
        primary,
        primary_alt,
        secondary,
        secondary_alt,
        tertiary,
        tertiary_alt,
        contrasts,
    }: &Props,
) -> Html {
    let primary_alt_triadic = use_local_storage::<u16>("primary_alt_triadic".to_string());
    let secondary_triadic = use_local_storage::<u16>("secondary_triadic".to_string());
    let secondary_alt_triadic = use_local_storage::<u16>("secondary_alt_triadic".to_string());
    let tertiary_triadic = use_local_storage::<u16>("tertiary_triadic".to_string());
    let tertiary_alt_triadic = use_local_storage::<u16>("tertiary_alt_triadic".to_string());

    let set_primary = Callback::from({
        let set_primary = primary.setter.clone();

        let primary_alt = primary_alt.clone();
        let set_primary_alt = primary_alt.setter.clone();

        let secondary = secondary.clone();
        let set_secondary = secondary.setter.clone();

        let secondary_alt = secondary_alt.clone();
        let set_secondary_alt = secondary_alt.setter.clone();

        let tertiary = tertiary.clone();
        let set_tertiary = tertiary.setter.clone();

        let tertiary_alt = tertiary_alt.clone();
        let set_tertiary_alt = tertiary_alt.setter.clone();

        let primary_alt_triadic = primary_alt_triadic.clone();
        let secondary_triadic = secondary_triadic.clone();
        let secondary_alt_triadic = secondary_alt_triadic.clone();
        let tertiary_triadic = tertiary_triadic.clone();
        let tertiary_alt_triadic = tertiary_alt_triadic.clone();

        move |Hsl {
                  hue,
                  saturation,
                  lightness,
              }| {
            set_primary.emit(Hsl {
                hue,
                saturation,
                lightness,
            });
            set_primary_alt.emit(Hsl {
                hue: (hue + primary_alt_triadic.unwrap_or(PRIMARY_ALT_TRIADIC)) % 360,
                ..primary_alt.hsl
            });

            set_secondary.emit(Hsl {
                hue: (hue + secondary_triadic.unwrap_or(SECONDARY_TRIADIC)) % 360,
                ..secondary.hsl
            });
            set_secondary_alt.emit(Hsl {
                hue: (hue + secondary_alt_triadic.unwrap_or(SECONDARY_ALT_TRIADIC)) % 360,
                ..secondary_alt.hsl
            });

            set_tertiary.emit(Hsl {
                hue: (hue + tertiary_triadic.unwrap_or(TERTIARY_TRIADIC)) % 360,
                ..tertiary.hsl
            });
            set_tertiary_alt.emit(Hsl {
                hue: (hue + tertiary_alt_triadic.unwrap_or(TERTIARY_ALT_TRIADIC)) % 360,
                ..tertiary_alt.hsl
            });
        }
    });

    let set_primary_preview = Callback::from({
        let set_primary_preview = primary.previewer.clone();

        let set_primary_alt_preview = primary_alt.previewer.clone();
        let primary_alt = primary_alt.hsl.clone();

        let set_secondary_preview = secondary.previewer.clone();
        let secondary = secondary.clone();

        let set_secondary_alt_preview = secondary_alt.previewer.clone();
        let secondary_alt = secondary_alt.hsl.clone();

        let set_tertiary_preview = tertiary_alt.previewer.clone();
        let tertiary = tertiary.hsl.clone();

        let set_tertiary_alt_preview = tertiary_alt.previewer.clone();
        let tertiary_alt = tertiary_alt.hsl.clone();

        let primary_alt_triadic = primary_alt_triadic.clone();
        let secondary_triadic = secondary_triadic.clone();
        let secondary_alt_triadic = secondary_alt_triadic.clone();
        let tertiary_triadic = tertiary_triadic.clone();
        let tertiary_alt_triadic = tertiary_alt_triadic.clone();

        move |option_hsl| {
            if let Some(Hsl { hue, .. }) = option_hsl {
                set_primary_preview.emit(option_hsl);
                set_primary_alt_preview.emit(Some(Hsl {
                    hue: (hue + primary_alt_triadic.unwrap_or(PRIMARY_ALT_TRIADIC)) % 360,
                    ..primary_alt
                }));
                set_secondary_preview.emit(Some(Hsl {
                    hue: (hue + secondary_triadic.unwrap_or(SECONDARY_TRIADIC)) % 360,
                    ..secondary.hsl
                }));
                set_secondary_alt_preview.emit(Some(Hsl {
                    hue: (hue + secondary_alt_triadic.unwrap_or(SECONDARY_ALT_TRIADIC)) % 360,
                    ..secondary_alt
                }));
                set_tertiary_preview.emit(Some(Hsl {
                    hue: (hue + tertiary_triadic.unwrap_or(TERTIARY_TRIADIC)) % 360,
                    ..tertiary
                }));
                set_tertiary_alt_preview.emit(Some(Hsl {
                    hue: (hue + tertiary_alt_triadic.unwrap_or(TERTIARY_ALT_TRIADIC)) % 360,
                    ..tertiary_alt
                }));
            } else {
                set_primary_preview.emit(None);
                set_primary_alt_preview.emit(None);
                set_secondary_preview.emit(None);
                set_secondary_alt_preview.emit(None);
                set_tertiary_preview.emit(None);
                set_tertiary_alt_preview.emit(None);
            }
        }
    });

    html! {
        <article class={classes!("page", "triadic", "flow")}>
            <header>
                <h1>{"Triadic"}</h1>
            </header>

            <section class={classes!("backgrounds", "flow")}>
                <h2>{"Backgrounds"}</h2>

                <section class={classes!("primary")}>
                    <h3>{"Primary"}</h3>

                    <blockquote>
                        <pre>
                            if let Some(ref primary) = primary.preview {
                                {format!("--primary: hsl({}, {}%, {}%);", primary.hue, primary.saturation, primary.lightness)}
                            } else {
                                {format!("--primary: hsl({}, {}%, {}%);", primary.hsl.hue, primary.hsl.saturation, primary.hsl.lightness)}
                            }
                        </pre>

                        <p>{format!("Contrast is {:.2}:1 with foreground and {:.2}:1 with accent.", contrasts.primary_secondary, contrasts.primary_tertiary)}</p>

                        <ul>
                            <li>{"For body text, 4.5:1 is the minimum ratio while 7:1 is for enhanced accessibility."}</li>
                            <li>{"For large text, 3:1 is the minimum ratio while 4.5:1 is for enhanced accessibility."}</li>
                        </ul>
                    </blockquote>

                    <div>
                        <p>{"Pick the primary color :"}</p>
                        <Hue hsl={primary.hsl.clone()} set_hsl={set_primary.clone()} set_preview={set_primary_preview.clone()} />
                    </div>

                    <div>
                        <p>{"Tune its saturation :"}</p>
                        <Saturation color={primary.clone()} />
                    </div>

                    <div>
                        <p>{"And its lightness :"}</p>
                        <Lightness color={primary.clone()} />
                    </div>
                </section>

                <section class={classes!("primary", "primary--alt", "flow")}>
                    <h3>{"Primary Alt"}</h3>

                    <blockquote>
                        <pre>
                            if let Some(ref primary_alt) = primary_alt.preview {
                                {format!("--primary-alt: hsl({}, {}%, {}%);", primary_alt.hue, primary_alt.saturation, primary_alt.lightness)}
                            } else {
                                {format!("--primary-alt: hsl({}, {}%, {}%);", primary_alt.hsl.hue, primary_alt.hsl.saturation, primary_alt.hsl.lightness)}
                            }
                        </pre>

                        <p>{format!("Contrast is {:.2}:1 with alt foreground and {:.2}:1 with alt accent.", contrasts.primary_alt_secondary_alt, contrasts.primary_alt_tertiary_alt)}</p>

                        <ul>
                            <li>{"For body text, 4.5:1 is the minimum ratio while 7:1 is for enhanced accessibility."}</li>
                            <li>{"For large text, 3:1 is the minimum ratio while 4.5:1 is for enhanced accessibility."}</li>
                        </ul>
                    </blockquote>

                    <div>
                        <p>{"Pick one of the three triadic colors."}</p>
                        <utils::Utils primary={primary.clone()} color={primary_alt.clone()} />
                    </div>

                    <div>
                        <p>{"Tune its saturation :"}</p>
                        <Saturation color={primary_alt.clone()} />
                    </div>

                    <div>
                        <p>{"And its lightness :"}</p>
                        <Lightness color={primary_alt.clone()} />
                    </div>
                </section>
            </section>

            <section class={classes!("foregrounds", "flow")}>
                <h2>{"Foregrounds"}</h2>

                <section class={classes!("secondary", "flow")}>
                    <h3>{"Secondary"}</h3>

                    <blockquote>
                        <pre>
                            if let Some(ref secondary) = secondary.preview {
                                {format!("--secondary: hsl({}, {}%, {}%);", secondary.hue, secondary.saturation, secondary.lightness)}
                            } else {
                                {format!("--secondary: hsl({}, {}%, {}%);", secondary.hsl.hue, secondary.hsl.saturation, secondary.hsl.lightness)}
                            }
                        </pre>

                        <p>{format!("Contrast is {:.2}:1 with background.", contrasts.primary_secondary)}</p>

                        <ul>
                            <li>{"For body text, 4.5:1 is the minimum ratio while 7:1 is for enhanced accessibility."}</li>
                            <li>{"For large text, 3:1 is the minimum ratio while 4.5:1 is for enhanced accessibility."}</li>
                        </ul>
                    </blockquote>

                    <div>
                        <p>{"Pick one of the three triadic colors."}</p>
                        <utils::Utils primary={primary.clone()} color={secondary.clone()} />
                    </div>

                    <div>
                        <p>{"Tune its saturation :"}</p>
                        <Saturation color={secondary.clone()} />
                    </div>

                    <div>
                        <p>{"And its lightness :"}</p>
                        <Lightness color={secondary.clone()} />
                    </div>
                </section>

                <section class={classes!("secondary", "secondary--alt", "flow")}>
                    <h3>{"Secondary Alt"}</h3>

                    <blockquote>
                        <pre>
                            if let Some(ref secondary_alt) = secondary_alt.preview {
                                {format!("--secondary-alt: hsl({}, {}%, {}%);", secondary_alt.hue, secondary_alt.saturation, secondary_alt.lightness)}
                            } else {
                                {format!("--secondary-alt: hsl({}, {}%, {}%);", secondary_alt.hsl.hue, secondary_alt.hsl.saturation, secondary_alt.hsl.lightness)}
                            }
                        </pre>

                        <p>{format!("Contrast is {:.2}:1 with alt background.", contrasts.primary_alt_secondary_alt)}</p>

                        <ul>
                            <li>{"For body text, 4.5:1 is the minimum ratio while 7:1 is for enhanced accessibility."}</li>
                            <li>{"For large text, 3:1 is the minimum ratio while 4.5:1 is for enhanced accessibility."}</li>
                        </ul>
                    </blockquote>

                    <div>
                        <p>{"Pick one of the three triadic colors."}</p>
                        <utils::Utils primary={primary.clone()} color={secondary_alt.clone()} />
                    </div>

                    <div>
                        <p>{"Tune its saturation :"}</p>
                        <Saturation color={secondary_alt.clone()} />
                    </div>

                    <div>
                        <p>{"And its lightness :"}</p>
                        <Lightness color={secondary_alt.clone()} />
                    </div>
                </section>
            </section>

            <section class={classes!("accents", "flow")}>
                <h2>{"Accents"}</h2>

                <section class={classes!("tertiary", "flow")}>
                    <h3>{"Tertiary"}</h3>

                    <blockquote>
                        <pre>
                            if let Some(ref tertiary) = tertiary.preview {
                                {format!("--tertiary: hsl({}, {}%, {}%);", tertiary.hue, tertiary.saturation, tertiary.lightness)}
                            } else {
                                {format!("--tertiary: hsl({}, {}%, {}%);", tertiary.hsl.hue, tertiary.hsl.saturation, tertiary.hsl.lightness)}
                            }
                        </pre>

                        <p>{format!("Contrast is {:.2}:1 with background.", contrasts.primary_tertiary)}</p>

                        <ul>
                            <li>{"For body text, 4.5:1 is the minimum ratio while 7:1 is for enhanced accessibility."}</li>
                            <li>{"For large text, 3:1 is the minimum ratio while 4.5:1 is for enhanced accessibility."}</li>
                        </ul>
                    </blockquote>

                    <div>
                        <p>{"Pick one of the three triadic colors."}</p>
                        <utils::Utils primary={primary.clone()} color={tertiary.clone()} />
                    </div>

                    <div>
                        <p>{"Tune its saturation :"}</p>
                        <Saturation color={tertiary.clone()} />
                    </div>

                    <div>
                        <p>{"And its lightness :"}</p>
                        <Lightness color={tertiary.clone()} />
                    </div>
                </section>

                <section class={classes!("tertiary", "tertiary--alt", "flow")}>
                    <h3>{"Tertiary Alt"}</h3>

                    <blockquote>
                        <pre>
                            if let Some(ref tertiary_alt) = tertiary_alt.preview {
                                {format!("--tertiary-alt: hsl({}, {}%, {}%);", tertiary_alt.hue, tertiary_alt.saturation, tertiary_alt.lightness)}
                            } else {
                                {format!("--tertiary-alt: hsl({}, {}%, {}%);", tertiary_alt.hsl.hue, tertiary_alt.hsl.saturation, tertiary_alt.hsl.lightness)}
                            }
                        </pre>

                        <p>{format!("Contrast is {:.2}:1 with alt background.", contrasts.primary_alt_tertiary_alt)}</p>

                        <ul>
                            <li>{"For body text, 4.5:1 is the minimum ratio while 7:1 is for enhanced accessibility."}</li>
                            <li>{"For large text, 3:1 is the minimum ratio while 4.5:1 is for enhanced accessibility."}</li>
                        </ul>
                    </blockquote>

                    <div>
                        <p>{"Pick one of the three triadic colors."}</p>
                        <utils::Utils primary={primary.clone()} color={tertiary_alt.clone()} />
                    </div>

                    <div>
                        <p>{"Tune its saturation :"}</p>
                        <Saturation color={tertiary_alt.clone()} />
                    </div>

                    <div>
                        <p>{"And its lightness :"}</p>
                        <Lightness color={tertiary_alt.clone()} />
                    </div>
                </section>
            </section>
        </article>
    }
}

#[derive(Properties, PartialEq)]
pub struct Props {
    pub primary: Color,
    pub primary_alt: Color,
    pub secondary: Color,
    pub secondary_alt: Color,
    pub tertiary: Color,
    pub tertiary_alt: Color,
    pub contrasts: Contrasts,
}

mod utils {
    use crate::{
        colors::{Hsl, Luminance, Rgb},
        Color,
    };
    use yew::prelude::*;

    #[function_component]
    pub fn Utils(Props { primary, color }: &Props) -> Html {
        let onmouseenter = |shift: u16| {
            let hue = (primary.hsl.hue + shift) % 360;
            let color = color.clone();
            Callback::from(move |_| color.previewer.emit(Some(Hsl { hue, ..color.hsl })))
        };

        let onmmouseleave = Callback::from({
            let color = color.clone();
            Callback::from(move |_| color.previewer.emit(None))
        });

        let onclick = |shift: u16| {
            let hue = (primary.hsl.hue + shift) % 360;
            let color = color.clone();
            Callback::from(move |_| color.setter.emit(Hsl { hue, ..color.hsl }))
        };

        let left = (primary.preview.as_ref().unwrap_or(&primary.hsl).hue + 240) % 360;
        let center = primary.preview.as_ref().unwrap_or(&primary.hsl).hue;
        let right = (primary.preview.as_ref().unwrap_or(&primary.hsl).hue + 120) % 360;

        let left_contrast = Luminance::from(Rgb::from(Hsl {
            hue: left,
            ..color.preview.as_ref().unwrap_or(&color.hsl).clone()
        }))
        .contrast(&Luminance(0f32));
        let center_contrast = Luminance::from(Rgb::from(Hsl {
            hue: center,
            ..color.preview.as_ref().unwrap_or(&color.hsl).clone()
        }))
        .contrast(&Luminance(0f32));
        let right_contrast = Luminance::from(Rgb::from(Hsl {
            hue: right,
            ..color.preview.as_ref().unwrap_or(&color.hsl).clone()
        }))
        .contrast(&Luminance(0f32));

        html! {
            <div class={
                classes!("utils",
                format!("sat--{}", color.preview.as_ref().unwrap_or(&color.hsl).saturation),
                format!("lig--{}", color.preview.as_ref().unwrap_or(&color.hsl).lightness),
            )}>
                <span class={classes!("hsl", if left_contrast < 10.5 { "color--white" } else { "color--black" }, format!("hue--{left}"))} onmouseenter={onmouseenter(240)} onmouseout={onmmouseleave.clone()} onclick={onclick(240)} >{left.to_string()}</span>
                <span class={classes!("hsl", if center_contrast < 10.5 { "color--white" } else { "color--black" }, format!("hue--{center}"))} onmouseenter={onmouseenter(0)} onmouseout={onmmouseleave.clone()} onclick={onclick(0)} >{center.to_string()}</span>
                <span class={classes!("hsl", if right_contrast < 10.5 { "color--white" } else { "color--black" }, format!("hue--{right}"))} onmouseenter={onmouseenter(120)} onmouseout={onmmouseleave.clone()} onclick={onclick(120)} >{right.to_string()}</span>
            </div>
        }
    }

    #[derive(Properties, PartialEq)]
    pub struct Props {
        pub primary: Color,
        pub color: Color,
    }
}
