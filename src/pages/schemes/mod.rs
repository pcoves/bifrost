mod analogous;
mod complementary;
mod compound;
mod hue;
mod lightness;
mod monochromatic;
mod saturation;
mod triadic;

use self::{
    analogous::Analogous, complementary::Complementary, compound::Compound,
    monochromatic::Monochromatic, triadic::Triadic,
};
use crate::{Color, Contrasts};
use yew::prelude::*;
use yew_router::prelude::*;

#[derive(Clone, Routable, PartialEq)]
pub enum Route {
    #[at("/schemes/monochromatic")]
    Monochromatic,

    #[at("/schemes/analogous")]
    Analogous,

    #[at("/schemes/complementary")]
    Complementary,

    #[at("/schemes/triadic")]
    Triadic,

    #[at("/schemes/compound")]
    Compound,

    #[not_found]
    #[at("/schemes/404")]
    NotFound,
}

impl Route {
    pub fn switch(
        primary: Color,
        primary_alt: Color,
        secondary: Color,
        secondary_alt: Color,
        tertiary: Color,
        tertiary_alt: Color,
        contrasts: Contrasts,
    ) -> impl Fn(Route) -> Html {
        move |route| match route {
            Self::Monochromatic => {
                html! {
                    <Monochromatic
                        primary={primary.clone()} primary_alt={primary_alt.clone()}
                        secondary={secondary.clone()} secondary_alt={secondary_alt.clone()}
                        tertiary={tertiary.clone()} tertiary_alt={tertiary_alt.clone()}
                        contrasts={contrasts.clone()}
                    />
                }
            }

            Self::Analogous => {
                html! {
                    <Analogous
                        primary={primary.clone()} primary_alt={primary_alt.clone()}
                        secondary={secondary.clone()} secondary_alt={secondary_alt.clone()}
                        tertiary={tertiary.clone()} tertiary_alt={tertiary_alt.clone()}
                        contrasts={contrasts.clone()}
                    />
                }
            }

            Self::Complementary => {
                html! {
                    <Complementary
                        primary={primary.clone()} primary_alt={primary_alt.clone()}
                        secondary={secondary.clone()} secondary_alt={secondary_alt.clone()}
                        tertiary={tertiary.clone()} tertiary_alt={tertiary_alt.clone()}
                        contrasts={contrasts.clone()}
                    />
                }
            }

            Self::Triadic => {
                html! {
                    <Triadic
                        primary={primary.clone()} primary_alt={primary_alt.clone()}
                        secondary={secondary.clone()} secondary_alt={secondary_alt.clone()}
                        tertiary={tertiary.clone()} tertiary_alt={tertiary_alt.clone()}
                        contrasts={contrasts.clone()}
                    />
                }
            }

            Self::Compound => {
                html! {
                    <Compound
                        primary={primary.clone()} primary_alt={primary_alt.clone()}
                        secondary={secondary.clone()} secondary_alt={secondary_alt.clone()}
                        tertiary={tertiary.clone()} tertiary_alt={tertiary_alt.clone()}
                        contrasts={contrasts.clone()}
                    />
                }
            }

            Self::NotFound => html! {<Redirect<crate::Route> to={crate::Route::NotFound}/>},
        }
    }
}
