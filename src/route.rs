use crate::Contrasts;

use super::{pages::*, Color};
use yew::prelude::*;
use yew_router::prelude::*;

#[derive(Clone, Routable, PartialEq)]
pub enum Route {
    #[at("/")]
    Root,

    #[at("/contrast")]
    Contrast,

    #[at("/schemes/*")]
    Schemes,

    #[not_found]
    #[at("/404")]
    NotFound,
}

impl Route {
    pub fn switch(
        primary: Color,
        primary_alt: Color,
        secondary: Color,
        secondary_alt: Color,
        tertiary: Color,
        tertiary_alt: Color,
        contrasts: Contrasts,
    ) -> impl Fn(Route) -> Html {
        move |routes| match routes {
            Route::Root => {
                html! { <Root /> }
            }
            Route::Contrast => {
                html! { <Contrast /> }
            }
            Route::Schemes => {
                html! { <Switch<schemes::Route> render={schemes::Route::switch(
                    primary.clone()     ,
                    primary_alt.clone() ,
                    secondary.clone()    ,
                    secondary_alt.clone(),
                    tertiary.clone()    ,
                    tertiary_alt.clone(),
                    contrasts.clone(),
                )} /> }
            }
            Route::NotFound => html! { <NotFound /> },
        }
    }
}
