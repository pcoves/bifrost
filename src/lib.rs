mod colors;
mod footer;
mod header;
mod nav;
mod pages;
mod route;

use self::{colors::Hsl, footer::Footer, header::Header, nav::Nav, route::Route};
use crate::colors::{Luminance, Rgb};
use yew::prelude::*;
use yew_hooks::{use_local_storage, UseLocalStorageHandle};
use yew_router::prelude::*;

const PRIMARY: Hsl = Hsl {
    hue: 200,
    saturation: 30,
    lightness: 90,
};

const PRIMARY_ALT: Hsl = Hsl {
    hue: 200,
    saturation: 40,
    lightness: 80,
};

const SECONDARY: Hsl = Hsl {
    hue: 200,
    saturation: 25,
    lightness: 20,
};

const SECONDARY_ALT: Hsl = Hsl {
    hue: 200,
    saturation: 10,
    lightness: 35,
};

const TERTIARY: Hsl = Hsl {
    hue: 200,
    saturation: 65,
    lightness: 50,
};

const TERTIARY_ALT: Hsl = Hsl {
    hue: 200,
    saturation: 65,
    lightness: 50,
};

#[function_component]
pub fn App() -> Html {
    let primary = use_local_storage::<Hsl>("primary".to_string());

    let set_primary = Callback::from({
        let primary = primary.clone();
        move |hsl| primary.set(hsl)
    });

    let primary_preview = use_state(Option::<Hsl>::default);

    let set_primary_preview = Callback::from({
        let primary_preview = primary_preview.clone();
        move |option_hsl| primary_preview.set(option_hsl)
    });

    let primary_alt = use_local_storage::<Hsl>("primary_alt".to_string());

    let set_primary_alt = Callback::from({
        let primary_alt = primary_alt.clone();
        move |hsl| primary_alt.set(hsl)
    });

    let primary_alt_preview = use_state(Option::<Hsl>::default);

    let set_primary_alt_preview = Callback::from({
        let primary_alt_preview = primary_alt_preview.clone();
        move |option_hsl| primary_alt_preview.set(option_hsl)
    });

    let secondary = use_local_storage::<Hsl>("secondary".to_string());

    let set_secondary = Callback::from({
        let secondary = secondary.clone();
        move |hsl| secondary.set(hsl)
    });

    let secondary_preview = use_state(Option::<Hsl>::default);

    let set_secondary_preview = Callback::from({
        let secondary_preview = secondary_preview.clone();
        move |option_hsl| secondary_preview.set(option_hsl)
    });

    let secondary_alt = use_local_storage::<Hsl>("secondary_alt".to_string());

    let set_secondary_alt = Callback::from({
        let secondary_alt = secondary_alt.clone();
        move |hsl| secondary_alt.set(hsl)
    });

    let secondary_alt_preview = use_state(Option::<Hsl>::default);

    let set_secondary_alt_preview = Callback::from({
        let secondary_alt_preview = secondary_alt_preview.clone();
        move |option_hsl| secondary_alt_preview.set(option_hsl)
    });

    let tertiary = use_local_storage::<Hsl>("tertiary".to_string());

    let set_tertiary = Callback::from({
        let tertiary = tertiary.clone();
        move |hsl| tertiary.set(hsl)
    });

    let tertiary_preview = use_state(Option::<Hsl>::default);

    let set_tertiary_preview = Callback::from({
        let tertiary_preview = tertiary_preview.clone();
        move |option_hsl| tertiary_preview.set(option_hsl)
    });

    let tertiary_alt = use_local_storage::<Hsl>("tertiary_alt".to_string());

    let set_tertiary_alt = Callback::from({
        let tertiary_alt = tertiary_alt.clone();
        move |hsl| tertiary_alt.set(hsl)
    });

    let tertiary_alt_preview = use_state(Option::<Hsl>::default);

    let set_tertiary_alt_preview = Callback::from({
        let tertiary_alt_preview = tertiary_alt_preview.clone();
        move |option_hsl| tertiary_alt_preview.set(option_hsl)
    });

    let primary_secondary = use_state(|| 0f32);
    let primary_alt_secondary_alt = use_state(|| 0f32);
    let primary_tertiary = use_state(|| 0f32);
    let primary_alt_tertiary_alt = use_state(|| 0f32);

    let primary_contrast = use_state(|| {
        Luminance::from(Rgb::from((*primary).clone().unwrap_or(PRIMARY))).contrast(&Luminance(0f32))
    });
    let primary_alt_contrast = use_state(|| {
        Luminance::from(Rgb::from((*primary_alt).clone().unwrap_or(PRIMARY_ALT)))
            .contrast(&Luminance(0f32))
    });
    let secondary_contrast = use_state(|| {
        Luminance::from(Rgb::from((*secondary).clone().unwrap_or(SECONDARY)))
            .contrast(&Luminance(0f32))
    });
    let secondary_alt_contrast = use_state(|| {
        Luminance::from(Rgb::from((*secondary_alt).clone().unwrap_or(SECONDARY_ALT)))
            .contrast(&Luminance(0f32))
    });
    let tertiary_contrast = use_state(|| {
        Luminance::from(Rgb::from((*tertiary).clone().unwrap_or(TERTIARY)))
            .contrast(&Luminance(0f32))
    });
    let tertiary_alt_contrast = use_state(|| {
        Luminance::from(Rgb::from((*tertiary_alt).clone().unwrap_or(TERTIARY_ALT)))
            .contrast(&Luminance(0f32))
    });

    use_effect_with_deps(
        {
            let primary_contrast = primary_contrast.clone();
            move |primary: &UseLocalStorageHandle<Hsl>| {
                let Hsl {
                    hue,
                    saturation,
                    lightness,
                } = (*primary).as_ref().unwrap_or(&PRIMARY);

                let style = web_sys::Document::new().unwrap().body().unwrap().style();
                let _ = style.set_property("--primary-hue", &hue.to_string());
                let _ = style.set_property("--primary-sat", &format!("{saturation}%"));
                let _ = style.set_property("--primary-lig", &format!("{lightness}%"));

                primary_contrast.set(
                    Luminance::from(Rgb::from(Hsl {
                        hue: *hue,
                        saturation: *saturation,
                        lightness: *lightness,
                    }))
                    .contrast(&Luminance(0f32)),
                );
            }
        },
        primary.clone(),
    );

    use_effect_with_deps(
        {
            let primary_alt_contrast = primary_alt_contrast.clone();
            move |primary_alt: &UseLocalStorageHandle<Hsl>| {
                let Hsl {
                    hue,
                    saturation,
                    lightness,
                } = (*primary_alt).as_ref().unwrap_or(&PRIMARY_ALT);

                let style = web_sys::Document::new().unwrap().body().unwrap().style();
                let _ = style.set_property("--primary-alt-hue", &hue.to_string());
                let _ = style.set_property("--primary-alt-sat", &format!("{saturation}%"));
                let _ = style.set_property("--primary-alt-lig", &format!("{lightness}%"));

                primary_alt_contrast.set(
                    Luminance::from(Rgb::from(Hsl {
                        hue: *hue,
                        saturation: *saturation,
                        lightness: *lightness,
                    }))
                    .contrast(&Luminance(0f32)),
                );
            }
        },
        primary_alt.clone(),
    );

    use_effect_with_deps(
        {
            let secondary_contrast = secondary_contrast.clone();
            move |secondary: &UseLocalStorageHandle<Hsl>| {
                let Hsl {
                    hue,
                    saturation,
                    lightness,
                } = (*secondary).as_ref().unwrap_or(&SECONDARY);

                let style = web_sys::Document::new().unwrap().body().unwrap().style();
                let _ = style.set_property("--secondary-hue", &hue.to_string());
                let _ = style.set_property("--secondary-sat", &format!("{saturation}%"));
                let _ = style.set_property("--secondary-lig", &format!("{lightness}%"));

                secondary_contrast.set(
                    Luminance::from(Rgb::from(Hsl {
                        hue: *hue,
                        saturation: *saturation,
                        lightness: *lightness,
                    }))
                    .contrast(&Luminance(0f32)),
                );
            }
        },
        secondary.clone(),
    );

    use_effect_with_deps(
        {
            let secondary_alt_contrast = secondary_alt_contrast.clone();
            move |secondary_alt: &UseLocalStorageHandle<Hsl>| {
                let Hsl {
                    hue,
                    saturation,
                    lightness,
                } = (*secondary_alt).as_ref().unwrap_or(&SECONDARY_ALT);

                let style = web_sys::Document::new().unwrap().body().unwrap().style();
                let _ = style.set_property("--secondary-alt-hue", &hue.to_string());
                let _ = style.set_property("--secondary-alt-sat", &format!("{saturation}%"));
                let _ = style.set_property("--secondary-alt-lig", &format!("{lightness}%"));

                secondary_alt_contrast.set(
                    Luminance::from(Rgb::from(Hsl {
                        hue: *hue,
                        saturation: *saturation,
                        lightness: *lightness,
                    }))
                    .contrast(&Luminance(0f32)),
                );
            }
        },
        secondary_alt.clone(),
    );

    use_effect_with_deps(
        {
            let tertiary_contrast = tertiary_contrast.clone();
            move |tertiary: &UseLocalStorageHandle<Hsl>| {
                let Hsl {
                    hue,
                    saturation,
                    lightness,
                } = (*tertiary).as_ref().unwrap_or(&TERTIARY);

                let style = web_sys::Document::new().unwrap().body().unwrap().style();
                let _ = style.set_property("--tertiary-hue", &hue.to_string());
                let _ = style.set_property("--tertiary-sat", &format!("{saturation}%"));
                let _ = style.set_property("--tertiary-lig", &format!("{lightness}%"));

                tertiary_contrast.set(
                    Luminance::from(Rgb::from(Hsl {
                        hue: *hue,
                        saturation: *saturation,
                        lightness: *lightness,
                    }))
                    .contrast(&Luminance(0f32)),
                );
            }
        },
        tertiary.clone(),
    );

    use_effect_with_deps(
        {
            let tertiary_alt_contrast = tertiary_alt_contrast.clone();
            move |tertiary_alt: &UseLocalStorageHandle<Hsl>| {
                let Hsl {
                    hue,
                    saturation,
                    lightness,
                } = (*tertiary_alt).as_ref().unwrap_or(&TERTIARY_ALT);

                let style = web_sys::Document::new().unwrap().body().unwrap().style();
                let _ = style.set_property("--tertiary-alt-hue", &hue.to_string());
                let _ = style.set_property("--tertiary-alt-sat", &format!("{saturation}%"));
                let _ = style.set_property("--tertiary-alt-lig", &format!("{lightness}%"));

                tertiary_alt_contrast.set(
                    Luminance::from(Rgb::from(Hsl {
                        hue: *hue,
                        saturation: *saturation,
                        lightness: *lightness,
                    }))
                    .contrast(&Luminance(0f32)),
                );
            }
        },
        tertiary_alt.clone(),
    );

    use_effect_with_deps(
        {
            let primary = primary.clone();
            let primary_contrast = primary_contrast.clone();
            move |primary_preview| {
                let style = web_sys::Document::new().unwrap().body().unwrap().style();

                if let Some(Hsl {
                    hue,
                    saturation,
                    lightness,
                }) = primary_preview
                {
                    let _ = style.set_property("--primary-hue-preview", &hue.to_string());
                    let _ = style.set_property("--primary-sat-preview", &format!("{saturation}%"));
                    let _ = style.set_property("--primary-lig-preview", &format!("{lightness}%"));

                    primary_contrast.set(
                        Luminance::from(Rgb::from(Hsl {
                            hue: *hue,
                            saturation: *saturation,
                            lightness: *lightness,
                        }))
                        .contrast(&Luminance(0f32)),
                    );
                } else {
                    let _ = style.remove_property("--primary-hue-preview");
                    let _ = style.remove_property("--primary-sat-preview");
                    let _ = style.remove_property("--primary-lig-preview");

                    primary_contrast.set(
                        Luminance::from(Rgb::from((*primary).clone().unwrap_or(PRIMARY)))
                            .contrast(&Luminance(0f32)),
                    );
                }
            }
        },
        (*primary_preview).clone(),
    );

    use_effect_with_deps(
        {
            let primary_alt = primary_alt.clone();
            let primary_alt_contrast = primary_alt_contrast.clone();
            move |primary_alt_preview| {
                let style = web_sys::Document::new().unwrap().body().unwrap().style();

                if let Some(Hsl {
                    hue,
                    saturation,
                    lightness,
                }) = primary_alt_preview
                {
                    let _ = style.set_property("--primary-alt-hue-preview", &hue.to_string());
                    let _ =
                        style.set_property("--primary-alt-sat-preview", &format!("{saturation}%"));
                    let _ =
                        style.set_property("--primary-alt-lig-preview", &format!("{lightness}%"));

                    primary_alt_contrast.set(
                        Luminance::from(Rgb::from(Hsl {
                            hue: *hue,
                            saturation: *saturation,
                            lightness: *lightness,
                        }))
                        .contrast(&Luminance(0f32)),
                    );
                } else {
                    let _ = style.remove_property("--primary-alt-hue-preview");
                    let _ = style.remove_property("--primary-alt-sat-preview");
                    let _ = style.remove_property("--primary-alt-lig-preview");

                    primary_alt_contrast.set(
                        Luminance::from(Rgb::from((*primary_alt).clone().unwrap_or(PRIMARY_ALT)))
                            .contrast(&Luminance(0f32)),
                    );
                }
            }
        },
        (*primary_alt_preview).clone(),
    );

    use_effect_with_deps(
        {
            let secondary = secondary.clone();
            let secondary_contrast = secondary_contrast.clone();
            move |secondary_preview| {
                let style = web_sys::Document::new().unwrap().body().unwrap().style();

                if let Some(Hsl {
                    hue,
                    saturation,
                    lightness,
                }) = secondary_preview
                {
                    let _ = style.set_property("--secondary-hue-preview", &hue.to_string());
                    let _ =
                        style.set_property("--secondary-sat-preview", &format!("{saturation}%"));
                    let _ = style.set_property("--secondary-lig-preview", &format!("{lightness}%"));

                    secondary_contrast.set(
                        Luminance::from(Rgb::from(Hsl {
                            hue: *hue,
                            saturation: *saturation,
                            lightness: *lightness,
                        }))
                        .contrast(&Luminance(0f32)),
                    );
                } else {
                    let _ = style.remove_property("--secondary-hue-preview");
                    let _ = style.remove_property("--secondary-sat-preview");
                    let _ = style.remove_property("--secondary-lig-preview");

                    secondary_contrast.set(
                        Luminance::from(Rgb::from((*secondary).clone().unwrap_or(SECONDARY)))
                            .contrast(&Luminance(0f32)),
                    );
                }
            }
        },
        (*secondary_preview).clone(),
    );

    use_effect_with_deps(
        {
            let secondary_alt = secondary_alt.clone();
            let secondary_alt_contrast = secondary_alt_contrast.clone();
            move |secondary_alt_preview| {
                let style = web_sys::Document::new().unwrap().body().unwrap().style();

                if let Some(Hsl {
                    hue,
                    saturation,
                    lightness,
                }) = secondary_alt_preview
                {
                    let _ = style.set_property("--secondary-alt-hue-preview", &hue.to_string());
                    let _ = style
                        .set_property("--secondary-alt-sat-preview", &format!("{saturation}%"));
                    let _ =
                        style.set_property("--secondary-alt-lig-preview", &format!("{lightness}%"));

                    secondary_alt_contrast.set(
                        Luminance::from(Rgb::from(Hsl {
                            hue: *hue,
                            saturation: *saturation,
                            lightness: *lightness,
                        }))
                        .contrast(&Luminance(0f32)),
                    );
                } else {
                    let _ = style.remove_property("--secondary-alt-hue-preview");
                    let _ = style.remove_property("--secondary-alt-sat-preview");
                    let _ = style.remove_property("--secondary-alt-lig-preview");

                    secondary_alt_contrast.set(
                        Luminance::from(Rgb::from(
                            (*secondary_alt).clone().unwrap_or(SECONDARY_ALT),
                        ))
                        .contrast(&Luminance(0f32)),
                    );
                }
            }
        },
        (*secondary_alt_preview).clone(),
    );

    use_effect_with_deps(
        {
            let tertiary = tertiary.clone();
            let tertiary_contrast = tertiary_contrast.clone();
            move |tertiary_preview| {
                let style = web_sys::Document::new().unwrap().body().unwrap().style();

                if let Some(Hsl {
                    hue,
                    saturation,
                    lightness,
                }) = tertiary_preview
                {
                    let _ = style.set_property("--tertiary-hue-preview", &hue.to_string());
                    let _ = style.set_property("--tertiary-sat-preview", &format!("{saturation}%"));
                    let _ = style.set_property("--tertiary-lig-preview", &format!("{lightness}%"));

                    tertiary_contrast.set(
                        Luminance::from(Rgb::from(Hsl {
                            hue: *hue,
                            saturation: *saturation,
                            lightness: *lightness,
                        }))
                        .contrast(&Luminance(0f32)),
                    );
                } else {
                    let _ = style.remove_property("--tertiary-hue-preview");
                    let _ = style.remove_property("--tertiary-sat-preview");
                    let _ = style.remove_property("--tertiary-lig-preview");

                    tertiary_contrast.set(
                        Luminance::from(Rgb::from((*tertiary).clone().unwrap_or(TERTIARY)))
                            .contrast(&Luminance(0f32)),
                    );
                }
            }
        },
        (*tertiary_preview).clone(),
    );

    use_effect_with_deps(
        {
            let tertiary_alt = tertiary_alt.clone();
            let tertiary_alt_contrast = tertiary_alt_contrast.clone();
            move |tertiary_alt_preview| {
                let style = web_sys::Document::new().unwrap().body().unwrap().style();

                if let Some(Hsl {
                    hue,
                    saturation,
                    lightness,
                }) = tertiary_alt_preview
                {
                    let _ = style.set_property("--tertiary-alt-hue-preview", &hue.to_string());
                    let _ =
                        style.set_property("--tertiary-alt-sat-preview", &format!("{saturation}%"));
                    let _ =
                        style.set_property("--tertiary-alt-lig-preview", &format!("{lightness}%"));

                    tertiary_alt_contrast.set(
                        Luminance::from(Rgb::from(Hsl {
                            hue: *hue,
                            saturation: *saturation,
                            lightness: *lightness,
                        }))
                        .contrast(&Luminance(0f32)),
                    );
                } else {
                    let _ = style.remove_property("--tertiary-alt-hue-preview");
                    let _ = style.remove_property("--tertiary-alt-sat-preview");
                    let _ = style.remove_property("--tertiary-alt-lig-preview");

                    tertiary_alt_contrast.set(
                        Luminance::from(Rgb::from((*tertiary_alt).clone().unwrap_or(TERTIARY_ALT)))
                            .contrast(&Luminance(0f32)),
                    );
                }
            }
        },
        (*tertiary_alt_preview).clone(),
    );

    use_effect_with_deps(
        {
            let primary_secondary = primary_secondary.clone();
            move |(primary, primary_preview, secondary, secondary_preview): &(
                UseLocalStorageHandle<Hsl>,
                Option<Hsl>,
                UseLocalStorageHandle<Hsl>,
                Option<Hsl>,
            )| {
                let primary = primary_preview
                    .clone()
                    .unwrap_or((*primary).as_ref().unwrap_or(&PRIMARY).clone());
                let secondary = secondary_preview
                    .clone()
                    .unwrap_or((*secondary).as_ref().unwrap_or(&SECONDARY).clone());

                let contrast = Luminance::from(Rgb::from(primary))
                    .contrast(&Luminance::from(Rgb::from(secondary)));

                primary_secondary.set(contrast);
            }
        },
        (
            primary.clone(),
            (*primary_preview).clone(),
            secondary.clone(),
            (*secondary_preview).clone(),
        ),
    );

    use_effect_with_deps(
        {
            let primary_alt_secondary_alt = primary_alt_secondary_alt.clone();
            move |(primary_alt, primary_alt_preview, secondary_alt, secondary_alt_preview): &(
                UseLocalStorageHandle<Hsl>,
                Option<Hsl>,
                UseLocalStorageHandle<Hsl>,
                Option<Hsl>,
            )| {
                let primary_alt = primary_alt_preview
                    .clone()
                    .unwrap_or((*primary_alt).as_ref().unwrap_or(&PRIMARY_ALT).clone());
                let secondary_alt = secondary_alt_preview
                    .clone()
                    .unwrap_or((*secondary_alt).as_ref().unwrap_or(&SECONDARY_ALT).clone());

                let contrast = Luminance::from(Rgb::from(primary_alt))
                    .contrast(&Luminance::from(Rgb::from(secondary_alt)));

                primary_alt_secondary_alt.set(contrast);
            }
        },
        (
            primary_alt.clone(),
            (*primary_alt_preview).clone(),
            secondary_alt.clone(),
            (*secondary_alt_preview).clone(),
        ),
    );

    use_effect_with_deps(
        {
            let primary_tertiary = primary_tertiary.clone();
            move |(primary, primary_preview, tertiary, tertiary_preview): &(
                UseLocalStorageHandle<Hsl>,
                Option<Hsl>,
                UseLocalStorageHandle<Hsl>,
                Option<Hsl>,
            )| {
                let primary = primary_preview
                    .clone()
                    .unwrap_or((*primary).as_ref().unwrap_or(&PRIMARY).clone());
                let tertiary = tertiary_preview
                    .clone()
                    .unwrap_or((*tertiary).as_ref().unwrap_or(&TERTIARY).clone());

                let contrast = Luminance::from(Rgb::from(primary))
                    .contrast(&Luminance::from(Rgb::from(tertiary)));

                primary_tertiary.set(contrast);
            }
        },
        (
            primary.clone(),
            (*primary_preview).clone(),
            tertiary.clone(),
            (*tertiary_preview).clone(),
        ),
    );

    use_effect_with_deps(
        {
            let primary_alt_tertiary_alt = primary_alt_tertiary_alt.clone();
            move |(primary_alt, primary_alt_preview, tertiary_alt, tertiary_alt_preview): &(
                UseLocalStorageHandle<Hsl>,
                Option<Hsl>,
                UseLocalStorageHandle<Hsl>,
                Option<Hsl>,
            )| {
                let primary_alt = primary_alt_preview
                    .clone()
                    .unwrap_or((*primary_alt).as_ref().unwrap_or(&PRIMARY_ALT).clone());
                let tertiary_alt = tertiary_alt_preview
                    .clone()
                    .unwrap_or((*tertiary_alt).as_ref().unwrap_or(&TERTIARY_ALT).clone());

                let contrast = Luminance::from(Rgb::from(primary_alt))
                    .contrast(&Luminance::from(Rgb::from(tertiary_alt)));

                primary_alt_tertiary_alt.set(contrast);
            }
        },
        (
            primary_alt.clone(),
            (*primary_alt_preview).clone(),
            tertiary_alt.clone(),
            (*tertiary_alt_preview).clone(),
        ),
    );

    html! {
        <BrowserRouter>
            <header><Header /></header>
            <nav><Nav /></nav>
            <main>
                <Switch<Route> render={
                    Route::switch(
                        Color{
                            hsl: (*primary).clone().unwrap_or(PRIMARY),
                            preview: (*primary_preview).clone(),
                            previewer: set_primary_preview,
                            setter: set_primary.clone(),
                            contrast: *primary_contrast
                        },
                        Color{
                            hsl: (*primary_alt).clone().unwrap_or(PRIMARY_ALT),
                            preview: (*primary_alt_preview).clone(),
                            previewer: set_primary_alt_preview,
                            setter: set_primary_alt.clone(),
                            contrast: *primary_alt_contrast
                        },
                        Color{
                            hsl: (*secondary).clone().unwrap_or(SECONDARY),
                            preview: (*secondary_preview).clone(),
                            previewer: set_secondary_preview,
                            setter: set_secondary.clone(),
                            contrast: *secondary_contrast
                        },
                        Color{
                            hsl: (*secondary_alt).clone().unwrap_or(SECONDARY_ALT),
                            preview: (*secondary_alt_preview).clone(),
                            previewer: set_secondary_alt_preview,
                            setter: set_secondary_alt.clone(),
                            contrast: *secondary_alt_contrast
                        },
                        Color{
                            hsl: (*tertiary).clone().unwrap_or(TERTIARY),
                            preview: (*tertiary_preview).clone(),
                            previewer: set_tertiary_preview,
                            setter: set_tertiary.clone(),
                            contrast: *tertiary_contrast,
                        },
                        Color{
                            hsl: (*tertiary_alt).clone().unwrap_or(TERTIARY_ALT),
                            preview: (*tertiary_alt_preview).clone(),
                            previewer: set_tertiary_alt_preview,
                            setter: set_tertiary_alt.clone(),
                            contrast: *tertiary_alt_contrast
                        },
                        Contrasts {
                            primary_secondary: *primary_secondary,
                            primary_alt_secondary_alt: *primary_alt_secondary_alt,
                            primary_tertiary: *primary_tertiary,
                            primary_alt_tertiary_alt: *primary_alt_tertiary_alt
                        },
                    )}
                />
             </main>
            <footer><Footer /></footer>
        </BrowserRouter>
    }
}

#[derive(Clone)]
pub struct Color {
    hsl: Hsl,
    preview: Option<Hsl>,
    previewer: Callback<Option<Hsl>>,
    setter: Callback<Hsl>,
    contrast: f32,
}

impl std::ops::Deref for Color {
    type Target = Hsl;

    fn deref(&self) -> &Self::Target {
        &self.hsl
    }
}

impl std::cmp::PartialEq for Color {
    fn eq(&self, other: &Self) -> bool {
        self.hsl.eq(&other.hsl)
            && self.preview.eq(&other.preview)
            && self.contrast.eq(&other.contrast)
    }
}

#[derive(Clone, PartialEq)]
pub struct Contrasts {
    primary_secondary: f32,
    primary_alt_secondary_alt: f32,
    primary_tertiary: f32,
    primary_alt_tertiary_alt: f32,
}
