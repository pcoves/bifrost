use bifrost::App;

fn main() {
    yew::Renderer::<App>::new().render();
}
