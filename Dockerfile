FROM rust:slim
RUN \
  rustup target add wasm32-unknown-unknown && \
  cargo install --locked trunk &&             \
  apt-get update &&                           \
  apt-get install -y binaryen &&              \
  apt-get clean &&                            \
  rm -rf /var/lib/apt/lists/*
