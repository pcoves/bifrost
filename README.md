# Bifrost

An enhanced version of [Rainbow](https://gitlab.com/pcoves/rainbow).

Visit at [Bifrost](https://pcoves.gitlab.io/bifrost) or run locally :

```bash
cargo install --locked trunk
trunk serve
```
